package be.smartschool.smscoauthtest

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.facebook.stetho.okhttp3.StethoInterceptor
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import java.io.IOException

data class VerifyPatformResponse(val isValid: Boolean)

data class AuthorizationTokens(
    val token_type: String,
    val access_token: String,
    val expires_in: Int,
    val refresh_token: String
)

interface OAuthService {

    @GET("/OAuth/verifyplatform")
    fun verifyPlatform(): Single<VerifyPatformResponse>

    @POST("/OAuth/mobile/token?client_id=123&client_secret=456&redirect_uri=smsc://&grant_type=authorization_code")
    fun getTokens(@Query("code") code: String, @Query("code_challenge") codeChallenge: String): Single<AuthorizationTokens>

    @GET("/Homepage/Menurpc/request?method=getItems")
    fun getMenuItems(): Single<Void>

    @POST("/OAuth/mobile/token?client_id=123&client_secret=456&redirect_uri=http://mobileredir1&grant_type=refresh_token")
    fun refreshToken(@Query("refresh_token") refreshToken: String): Single<AuthorizationTokens>
}

class MainActivity : AppCompatActivity() {

    companion object {
        //PKCE spec : https://tools.ietf.org/html/rfc7636
        //For easy demo purposes : https://tonyxu-io.github.io/pkce-generator/
        val CODE_VERIFIER = "B8YR6qfkZ39WDEce3oomcO0Aa5jIizJ3"
        val CODE_CHALLENGE = "_MTOsudvW-2u1kCr8JDCpBwfS9R9TawlYZBpHDMGoIM"
    }

    lateinit var retrofit: Retrofit
    lateinit var oAuthService: OAuthService
    var code: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.intent.data?.let {
            code = it.getQueryParameter("code")
            requestTokens()
        }

        btn.setOnClickListener {
            val domein = editText.text.toString()

            val okHttpClient = OkHttpClient.Builder().addNetworkInterceptor(StethoInterceptor()).build()

            retrofit = Retrofit.Builder()
                .baseUrl("https://$domein.smartschool.be")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

            oAuthService = retrofit.create(OAuthService::class.java)
            oAuthService.verifyPlatform()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        val url =
                            "https://$domein.smartschool.be/OAuth/mobile?client_id=123&response_type=code&scope=mobile&code_challenge_method=S256&code_challenge=$CODE_CHALLENGE&redirect_uri=smsc://"
                        val i = Intent(Intent.ACTION_VIEW)
                        i.data = Uri.parse(url)
                        startActivity(i)
                    }, { throwable ->
                        val s = ""
                    }
                )
        }

    }

    private fun requestTokens() {
        val okHttpClient = OkHttpClient.Builder().addNetworkInterceptor(StethoInterceptor()).build()

        retrofit = Retrofit.Builder()
            .baseUrl("https://testing9.smartschool.be")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        oAuthService = retrofit.create(OAuthService::class.java)
        code?.let {
            oAuthService.getTokens(it, CODE_CHALLENGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val s = ""
                   // getMenuItems(it.access_token)
                    getRefreshToken(it.refresh_token)
                }, {
                    val s = ""
                })
        }
    }

    private fun getRefreshToken(refreshToken: String) {
        val okHttpClient = OkHttpClient.Builder().addNetworkInterceptor(StethoInterceptor()).build()

        retrofit = Retrofit.Builder()
            .baseUrl("https://testing9.smartschool.be")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        oAuthService = retrofit.create(OAuthService::class.java)
        code?.let {
            oAuthService.refreshToken(it)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val s = it
                    //getMenuItems(it.access_token)
                    //getRefreshToken(it.refresh_token)
                }, {
                    val s = ""
                })
        }
    }

    @SuppressLint("CheckResult")
    private fun getMenuItems(access_token: String) {
        val client = OkHttpClient.Builder().addInterceptor(object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): Response {
                val newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer $access_token")
                    .build()
                return chain.proceed(newRequest)
            }
        }).addNetworkInterceptor(StethoInterceptor())

            .build()

        retrofit = Retrofit.Builder()
            .baseUrl("https://testing9.smartschool.be")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        oAuthService = retrofit.create(OAuthService::class.java)
        oAuthService.getMenuItems().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val s = ""

            }, {
                val s = ""
            })
    }
}
